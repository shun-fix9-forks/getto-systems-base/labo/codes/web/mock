require "sinatra"

configure do
  set :bind, "0.0.0.0"
end

options "*" do
  headers(
    "Access-Control-Allow-Origin" => "http://#{ENV["LABO_IP"]}:30080",
    "Access-Control-Allow-Methods" => "GET,POST,PUT,DELETE,OPTIONS,HEAD",
    "Access-Control-Allow-Headers" => "Authorization",
  )
end

get "/" do
  content_type 'application/json'
  { message: "hello, world!!" }.to_json
end

post '/upload' do
  headers(
    "Access-Control-Allow-Origin" => "http://#{ENV["LABO_IP"]}:30080",
    "Access-Control-Expose-Headers" => "X-Upload-ID",
  )

  halt 400 unless files = params[:text]

  buffer = ""

  files.each do |file|
    while blk = file[:tempfile].read(65536)
      buffer += blk
    end
  end

  headers["X-Upload-ID"] = "3"

  content_type 'application/json'
  { content: buffer, token: env["HTTP_AUTHORIZATION"].to_s }.to_json
end
